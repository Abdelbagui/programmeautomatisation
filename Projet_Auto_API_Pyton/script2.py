import requests
import os
import shutil
import re
import sys

websites = "https://my-json-server.typicode.com/tiko69/JSONserver/websites"
website = "https://my-json-server.typicode.com/tiko69/JSONserver/website?id="

def get_leaf_key_values(data, parent_key='', sep='.'):
    key_values = {}
    if parent_key == '[0]':
        parent_key = ''
    if isinstance(data, dict):
        is_leaf = not any(isinstance(v, dict) for v in data.values())
        for k, v in data.items():
            full_key = f"{parent_key}{sep}{k}" if parent_key else k
            if isinstance(v, dict):
                if not is_leaf:
                    key_values.update(get_leaf_key_values(v, full_key, sep))
            elif isinstance(v, list):
                for i, item in enumerate(v):
                    key_values.update(get_leaf_key_values(item, f"{full_key}[{i}]", sep))
            else:
                key_values[full_key] = v
    elif isinstance(data, list):
        for i, item in enumerate(data):
            key_values.update(get_leaf_key_values(item, f"{parent_key}[{i}]", sep))
    return key_values 

allWebsites = requests.get(websites)

if allWebsites.status_code != 200 :
    print("Error API")
    exit()

jsonAllWebsites = allWebsites.json()

for jsonWebsite in jsonAllWebsites:
    print(str(jsonWebsite["id"]) + " : " + jsonWebsite["website"])


if(len(sys.argv) > 1):
    choice = sys.argv[1]
else:
    choice = input("Website to build ? : ")

websiteToBuild = 0
for jsonWebsite in jsonAllWebsites:
    if int(choice) == int(jsonWebsite["id"]) :
        websiteToBuild = jsonWebsite

if os.path.exists("websites" + os.sep + websiteToBuild["website"]):
    shutil.rmtree("websites" + os.sep + websiteToBuild["website"])

os.mkdir("websites" + os.sep + websiteToBuild["website"])
shutil.copyfile("templates/index.html", "websites" + os.sep + websiteToBuild["website"] + os.sep + "index.html")
choosenWebsite = requests.get(website + str(websiteToBuild["id"]))
if choosenWebsite.status_code != 200 :
    print("Error API")
    exit()
   
choosenWebsiteData = get_leaf_key_values(choosenWebsite.json())

with open("websites" + os.sep + websiteToBuild["website"] + os.sep + "index.html", 'r') as file: 
  data = file.read()
    
for key, value in choosenWebsiteData.items():
    pattern = rf'\[{key}\]'
    data = re.sub(pattern, str(value), data)

with open("websites" + os.sep + websiteToBuild["website"] + os.sep + "index.html", 'w') as file: 
  file.write(data)

shutil.copyfile(f"assets/{choosenWebsiteData['img.src']}", "websites" + os.sep + websiteToBuild["website"] + os.sep + choosenWebsiteData['img.src'])
