import sys
import os
import re
import shutil
import requests


url1 = 'https://my-json-server.typicode.com/tiko69/JSONserver/websites'


############################les données reccuperées depuis le serveur json server dans le site url1 ############################
#Vérifier si la requête a réussi
reponse1 = requests.get(url1)
données = reponse1.json()


if reponse1.status_code == 200:

    print("les données récupérées sont :")

    for i in données:

         print(f"{i['id']} : {i['website']}") 

             
else:
    print("La requête a échoué avec le code :", reponse1.status_code)

############################### fin ############################################

#choisir le site que l'on veut selon le numero de site:

choix = input("veuillez entrer votre choix:" )

url2 = 'https://my-json-server.typicode.com/tiko69/JSONserver/website?id=' + str(choix)

############################les données reccuperées depuis le serveur json server dans le site url1 ############################

#Vérifier si la requête a réussi  
reponse2 = requests.get(url2)
données = reponse2.json()
dossier_destination= "projet_Auto_API" + os.sep + "websites" + os.sep + (données[0]['title']) 

##########creation d'un dossier avec le nom du site choisi ####################

if os.path.exists(dossier_destination):
    shutil.rmtree(dossier_destination)
os.mkdir(dossier_destination)

source = "projet_Auto_API" + os.sep + "templates" + os.sep + "index.html"
fichier_destination = dossier_destination + os.sep + "index.html"
shutil.copy(source, fichier_destination)

############################### fin #################################################
###modifieant le fichier index.html avec les données du site choisi #################

with open(fichier_destination, "r") as file:
    indexhtml = file.read()

indexhtml = indexhtml.replace("[title]", données[0]['title'])
indexhtml = indexhtml.replace("[body.color]", données[0]['body']['color'])
indexhtml = indexhtml.replace("[h1.background-color]", données[0]['h1']['background-color'])
indexhtml = indexhtml.replace(" [h1.color]", données[0]['h1']['color'])
indexhtml = indexhtml.replace("[p.color]", données[0]['p']['color'])
indexhtml = indexhtml.replace("[p.text]", données[0]['p']['text'])
indexhtml = indexhtml.replace("[h1.text]", données[0]['h1']['text'])
indexhtml = indexhtml.replace("[img.src]", données[0]['img']['src'])
indexhtml = indexhtml.replace("[img.alt]", données[0]['img']['alt'])

with open(fichier_destination, "w") as file: 

    file.write(indexhtml)

############################### fin #################################################

###########################copie de l'image dans le dossier du site choisi #################
source = "Projet_Auto_API" + os.sep + "assets" + os.sep + données[0]['img']['src']
chemin_dossier_img = dossier_destination  + os.sep + données[0]['img']['src']
shutil.copy(source, chemin_dossier_img)

