
########Nombres pairs et impairs##########

# Liste impairs
impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]

# Construire la liste pairs en incrémentant tous les éléments de impairs de 1

pairs = [ n + 1 for n in impairs]

# Afficher la liste pairs
print("Liste pairs:", pairs)
##################################
##################################
#conversion de liste paire en impaire
pairs = [2, 4, 6, 8, 10, 12, 14, 16, 18 ]

# Construire la liste impairs en incrémentant tous les éléments de pairs de 1
impairs = [ m - 1 for m in pairs]

# Afficher la liste impairs
print("Liste impairs:", impairs)

##########Calcul de la moyenne########