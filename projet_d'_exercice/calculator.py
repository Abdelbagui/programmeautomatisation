from tkinter import *
from tkinter import Button
win = Tk()
win.title('Calculator')
win.geometry('515x365')
win.resizable(0 , 0)

def btn_click(item):
    global expression
    expression = expression + str(item)
    input_text.set(expression)
expression = ""
input_text = StringVar()

# input field frame

input_frame = Frame(win, width=312, height=50)
input_frame.pack(side=TOP)

input_field = Entry(input_frame, font=('arial', 18, 'bold'), width=45, justify=RIGHT , textvariable=input_text)
input_field.grid(row=0, column=0)

#increase the height of the input field
input_pack.pack(ipady=10)

#Buttom frame
btns_frame = Frame(win, width=310, height=270)
btns_frame.pack()

clear = Button(btns_frame, text="C", width=38, height=3).grid(row=0, column=0, columnspan=3, padx=1, pady=1)
divide = Button(btns_frame, text="/", width=10, height=3).grid(row=0, column=3, columnspan=3, padx=1, pady=1)

#Botton second row 
seven = Botton(btns_frame, text="7", width=10, height=3, command=lamda: btn_click(7)).grid(row=1, column=0, padx=1, pady=1)
eight = Botton(btns_frame, text="8", width=10, height=3).grid(row=1, column=0, padx=1, pady=1)
nine = Botton(btns_frame, text="7", width=10, height=3).grid(row=1, column=0, padx=1, pady=1)
multiply = Botton(btns_frame, text="*", width=10, height=3).grid(row=1, column=0, padx=1, pady=1)

#Botton third row
four = Botton(btns_frame, text="4", width=10, height=3).grid(row=2, column=0, padx=1, pady=1)
five = Botton(btns_frame, text="5", width=10, height=3).grid(row=2, column=1, padx=1, pady=1)
six = Botton(btns_frame, text="6", width=10, height=3).grid(row=1, column=2, padx=1, pady=1)
minus = Botton(btns_frame, text="-", width=10, height=3).grid(row=1, column=3, padx=1, pady=1)

#Botton forth row
one = Botton(btns_frame, text="1", width=10, height=3).grid(row=3, column=0, padx=1, pady=1)
deux= Botton(btns_frame, text="2", width=10, height=3).grid(row=3, column=1, padx=1, pady=1)
tree = Botton(btns_frame, text="3", width=10, height=3).grid(row=3, column=2, padx=1, pady=1)
plus = Botton(btns_frame, text="+", width=10, height=3).grid(row=3, column=3, padx=1, pady=1)

#button fifth row

Zero = Botton(btns_frame, text="0", width=24, height=3).grid(row=4, column=0, columnspan=2, padx=1, pady=1)
point = Botton(btns_frame, text=".", width=10, height=3).grid(row=4, column=2, padx=1, pady=1)
equal = Botton(btns_frame, text="=", width=10, height=3).grid(row=4, column=3, padx=1, pady=1)

win.mainloop()
