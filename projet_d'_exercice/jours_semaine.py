"""""""""""exercice_JOUR_DE_LA_SEMAINE"""
#===========================PROGRMME DE JOUR DE LA SEMAINE======================================

liste_de_semaine = ["lundi" , "mardi" , "mercredi" , "jeudi" , "vendredi" , "samedi" , "dimanche"]

#À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours de la semaine d’une part, et ceux du week-end d’autre part ? Utilisez pour cela l’indiçage

print(liste_de_semaine)

#Cherchez un autre moyen pour arriver au même résultat (en utilisant un autre indiçage).

print(liste_de_semaine[0:2])
print(liste_de_semaine[:-2])

#Inversez les jours de la semaine en une commande.

