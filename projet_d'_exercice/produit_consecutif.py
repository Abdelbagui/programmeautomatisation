 ########Produit de nombres consécutifs#####
   # Créer la liste entiers contenant les nombres entiers pairs de 2 à 20 inclus
#liste = [1, 2, 3, 4, 5 , 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]


entiers = list(range(2, 21, 2))

# Afficher la liste entiers
print("Liste entiers:", entiers)

# Calculer le produit des nombres consécutifs deux à deux de entiers en utilisant une boucle
produit = 1
for i in range(len(entiers) - 1):
    produit *= entiers[i] * entiers[i + 1]

# Afficher le produit des nombres consécutifs deux à deux
print("Produit des nombres consécutifs deux à deux:", produit)